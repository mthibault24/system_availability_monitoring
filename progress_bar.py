class ProgressBar:
    completed_symbol = "#"
    remaining_symbol = "."

    @staticmethod
    def get(actual_progress, max_progress):
        progress_percentage = (actual_progress / max_progress) * 100
        remaining_progress = 100 - int(progress_percentage)
        progress_bar = "["
        for _ in range(0, int(progress_percentage)):
            progress_bar += ProgressBar.completed_symbol
        for _ in range(0, int(remaining_progress)):
            progress_bar += ProgressBar.remaining_symbol
        progress_bar += f"]({progress_percentage:.2f}%)({actual_progress}/{max_progress})"
        return progress_bar
