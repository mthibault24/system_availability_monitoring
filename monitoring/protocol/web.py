import requests
import urllib3
from .abstract import AbstractSystemAliveChecker

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class WebSystemAliveChecker(AbstractSystemAliveChecker):
    def check(self):
        _ = None
        try:
            response = requests.get(self.host, verify=False, timeout=3)
            if not response.ok:  # If response is not OK, it is an error!
                return _, response
            else:
                return response, _
        except Exception as e:
            return _, e
