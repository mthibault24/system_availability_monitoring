from pysnmp.hlapi import *
from .abstract import AbstractSystemAliveChecker


class SNMPSystemAliveChecker(AbstractSystemAliveChecker):

    def __init__(self, host):

        super().__init__(host)
        self.snmp_engine = SnmpEngine()
        self.community_data = CommunityData('public')  # SNMPv2c
        self.udp_transport_target = UdpTransportTarget((host, 161))
        self.context_data = ContextData()
        self.object_type = ObjectType(ObjectIdentity(('enterprises', 15000)))

        self.snmp_count_remembered = 0
        self.n_exec = 0

    def check(self):
        _ = None
        errors = dict()
        snmp_data_count = iter_count = 0

        for (error_indication, error_status, error_idx, var_binds) in nextCmd(
                self.snmp_engine,
                self.community_data,
                self.udp_transport_target,
                self.context_data,
                self.object_type,
                lexicographicMode=False
        ):
            # Count this increment
            iter_count += 1

            # If there's an error add it to the error list
            if error_indication:
                errors[iter_count] = (error_indication, error_status, error_idx)
                print(errors[iter_count])
            elif error_status:
                errors[iter_count] = '%s at %s' % (error_status.prettyPrint(),
                                                   error_idx and var_binds[int(error_idx) - 1][0] or '?')
                print(errors[iter_count])
            else:
                for var_bind in var_binds:
                    snmp_data_count += 1

        # Shoudn't have 0 record
        if snmp_data_count == 0:
            errors['No record'] = "No record has been found.."
            print(errors['No record'])

        if self.n_exec != 0:
            if self.snmp_count_remembered != snmp_data_count:
                errors['Bad count'] = f"Mismatch between record count - (iteration, count) : " \
                                      f"({self.n_exec}, {self.snmp_count_remembered}) " \
                                      "!= " \
                                      f"({self.n_exec+1}, {snmp_data_count})"
                print(errors['Bad count'])

        self.n_exec += 1
        self.snmp_count_remembered = snmp_data_count

        if errors:
            return _, errors
        else:
            return True, _

