from abc import ABC, abstractmethod


class AbstractSystemAliveChecker(ABC):
    """
    Abstract class to be implemented by stability checkers
    """

    def __init__(self, host):
        self.host = host

    @abstractmethod
    def check(self):
        pass
