from .protocol.web import WebSystemAliveChecker
from .protocol.snmp import SNMPSystemAliveChecker


class SystemAliveCheckerFactoryMethod:
    """
    Create a Stability Checker for a desired protocol
    """

    @staticmethod
    def make_web_system_alive_checker(host):
        return WebSystemAliveChecker(host)

    @staticmethod
    def make_snmp_system_alive_checker(host):
        return SNMPSystemAliveChecker(host)






