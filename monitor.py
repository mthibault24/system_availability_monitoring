import time
from progress_bar import ProgressBar
from report import Report


class Monitor:

    def __init__(self, host, duration, interval_delay, system_alive_checker):
        """
        :param host: what to monitor (ex. https://192.168.1.0)
        :param duration: how long should the monitor be executed in second
        :param interval_delay: time to wait between 2 check in second
        :param system_alive_checker: SystemAliveChecker to execute
        """
        self.host = host
        self.duration = duration
        self.interval_delay = interval_delay
        self.system_alive_checker = system_alive_checker

        self.errors = dict()
        self.nb_of_interval = int(duration / interval_delay)

        self.report = Report()
        self.report.add_config("host", host)
        self.report.add_config("duration (seconds)", duration)
        self.report.add_config("delay between interval (seconds)", interval_delay)
        self.report.add_config("total interval", self.nb_of_interval)

    def start(self):
        for current_interval in range(1, int(self.nb_of_interval) + 1):

            # PING
            response, error = self.system_alive_checker.check()

            if error is not None:
                self.report.add_error(current_interval, error)
                print(f"ERROR at {current_interval} : {error}")

            # PROGRESS
            progressBar = ProgressBar.get(current_interval, self.nb_of_interval)
            print(progressBar, end="\r")  # Display the progress bar
            time.sleep(self.interval_delay) if current_interval != self.nb_of_interval else None
            print(end="\x1b[2K")  # Delete the progress bar

        print("Done!")

        return self.report
