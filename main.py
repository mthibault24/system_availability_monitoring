import argparse
import datetime
from monitor import Monitor
from monitoring.provider import SystemAliveCheckerFactoryMethod

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--host", required=True, help="Target hostname, IP or URL (ex. https://localhost.com or 127.0.0.1 or localhost)")
    parser.add_argument("--protocol", default="web", type=str, choices=["web", "snmp"])
    parser.add_argument("--duration", default=3600, type=int, help="How long should the test run (seconds).")
    parser.add_argument("--interval", default=10, type=int, help="How long the test should wait before doing a recheck "
                                                                 "on the system (seconds).")
    args = parser.parse_args()

    print(f"Launching availability test on {args.host}")

    if args.protocol == "web":
        system_alive_checker = SystemAliveCheckerFactoryMethod.make_web_system_alive_checker(args.host)
    else:  # args.system is "snmp":
        system_alive_checker = SystemAliveCheckerFactoryMethod.make_snmp_system_alive_checker(args.host)

    monitor = Monitor(args.host, args.duration, args.interval, system_alive_checker)

    start_at = datetime.datetime.now()
    report = monitor.start()

    report.write(f"{args.protocol}_stability_test.log", start_at)
