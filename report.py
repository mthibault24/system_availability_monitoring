import datetime


class Report:

    def __init__(self):
        self.configuration = dict()
        self.errors = dict()

    def add_config(self, key, value):
        self.configuration[key] = value

    def add_error(self, key, value):
        self.errors[key] = value

    def write(self, app_name, start_at=None):
        # Write results
        end_at = datetime.datetime.now()
        filename = f"{app_name}.{end_at.strftime('%y%m%d%H%M')}.log"
        print(f"Writing results in {filename}")
        with open(filename, "w+") as file:
            if start_at is not None:
                file.write(f"Execution began at: {start_at}\n")
            file.write(f"Report written at: {end_at}\n\n")

            file.write(f"#################################################\n")
            file.write(f"                   CONFIGURATION                 \n")
            file.write(f"#################################################\n\n")

            for config_name, config_value in self.configuration.items():
                file.write(f"{config_name}: {config_value}\n")
            file.write("\n")

            file.write(f"#################################################\n")
            file.write(f"                   Results                       \n")
            file.write(f"#################################################\n\n")

            file.write(f"{len(self.errors)} error(s)\n\n")

            for err_id, err_msg in self.errors.items():
                file.write(f"ERROR at interval {err_id} :\n\t {err_msg}\n\n")
